const initialState = {
  login: '',
  taskText: '',
  tasks: [],
  nextTaskId: 1,
  isLoading: false
};

const reducer = (state=initialState, action) => {
  const newState = {...state};
  switch(action.type){
    case 'setLoading':
      newState.isLoading = action.value;
      break;
    case 'submitTaskCompleted':
      const tasksAfterSubmit = [...state.tasks, {id: state.nextTaskId, completed: false, task: action.resp.task}];
      newState.nextTaskId = state.nextTaskId + 1;
      newState.taskText = '';
      newState.tasks = tasksAfterSubmit;
      break;
    case 'removeTaskCompleted':
      const tasksAfterRemove = state.tasks.filter((task)=>{
        return task.id!==action.taskId
      })
      newState.tasks = tasksAfterRemove;
      break;
    case 'toggleTaskStatusCompleted':
      const tasksAfterToggle = state.tasks.map((task) => {
        if(task.id === action.resp.id)
        {
          task.completed = action.resp.completed
        }
        return task;
      });
      newState.tasks = tasksAfterToggle;
      break;
    case 'handleLoginChange':
      newState.login = action.event.target.value;
      break;
    case 'handleTaskChange':
      newState.taskText = action.event.target.value;
      break;
    case 'logout':
      newState.tasks = [];
      newState.nextTaskId = 1;
      newState.login = '';
      break;
    default:
      break;
  }
  return newState;
};

export default reducer;