import React from 'react';
import './App.css';
import LoginPage from './components/loginPage';
import TodosPage from './components/todosPage';
import AddTaskPage from './components/addTaskPage';
import LoadingDiv from './components/styled/loadingDiv';
import { Route } from 'react-router-dom'
import CircularProgress from '@material-ui/core/CircularProgress';
import { connect } from 'react-redux';

class App extends React.Component {
  render(){
    return (
      <div className="App">
        {this.props.isLoading && (
          <LoadingDiv>
            <CircularProgress/>
          </LoadingDiv>
        )}
        <Route 
          path="/" 
          exact 
          render={
            (props) => 
              <LoginPage 
                handleLoginChange = {this.props.handleLoginChange}
                {...props}
                />
          }
        />
        <Route 
          path="/todos" 
          exact 
          render={  
            (props) => 
              <TodosPage
                login = {this.props.login}
                tasks = {this.props.tasks}
                removeTask = {this.props.removeTask}
                toggleTaskStatus = {this.props.toggleTaskStatus}
                logout = {this.props.logout}
                {...props}
                />
          }
        />
        <Route 
          path="/addTask" 
          exact 
          render={  
            (props) => 
              <AddTaskPage
                handleTaskChange = {this.props.handleTaskChange}
                submitTask = {this.props.submitTask}
                {...props}
                />
          }
        />
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    login: state.login,
    taskText: state.taskText,
    tasks: state.tasks,
    nextTaskId: state.nextTaskId,
    isLoading: state.isLoading
  };
};

const mapDispachToProps = dispatch => {
  return {
    handleLoginChange: (event) => dispatch({ type: "handleLoginChange", event}),
    handleTaskChange: (event) => dispatch({ type: "handleTaskChange", event}),
    logout: () => dispatch({ type: "logout"}),
    submitTask: () => dispatch({ type: "submitTask"}),
    toggleTaskStatus: (taskId, event) => dispatch({ type: "toggleTaskStatus", taskId, event}),
    removeTask: (taskId) => dispatch({ type: "removeTask", taskId}),
  };
};

export default connect(
  mapStateToProps,
  mapDispachToProps
)(App);