import React from 'react';
import { Link } from 'react-router-dom'
import Card from './styled/card'
import AddButton from './styled/addButton'
import LogoutButton from './styled/logoutButton'
import Button from './styled/button'
import NavBar from './styled/navBar'
import constants from '../constants'
import TaskNumber from './styled/taskNumber';
import TaskContent from './styled/taskContent';

class TodosPage extends React.Component {
  render() {
    const renderTasks = this.props.tasks.map((task) => (
      <Card key={task.id}>
        <table style={{tableLayout: 'fixed', width:'100%'}}>
          <tbody>
            <tr>
              <td style={{width: '60px'}}>
                <TaskNumber>{task.id}</TaskNumber>
              </td>
              <td style={{width: 'auto'}}>
                <TaskContent>{task.task}</TaskContent>
              </td>
              <td style={{width: '120px'}}>
                <label style={{color:'grey'}}><input type="checkbox" checked={task.completed} onChange={(e)=>this.props.toggleTaskStatus(task.id, e)}/> Zrobione</label>
              </td>
              <td style={{width: '210px'}}>
                <Button onClick={()=>this.props.removeTask(task.id)} style={{float:'right'}}>Usuń</Button>
              </td>
            </tr>
          </tbody>
        </table>
      </Card>
      )
    );
    return (
      <>
        <NavBar>
          <LogoutButton as={Link} to="/" onClick={this.props.logout}>Wyloguj się</LogoutButton>
          Zalogowano jako: {this.props.login}
        </NavBar>
        {this.props.tasks.length === 0 && (
          <div>Brak zadań - najpierw jakieś dodaj!</div>
        )}
        {this.props.tasks.length !== 0  &&(
          <div>{renderTasks}</div>
        )}
        {this.props.tasks.length < constants.maxAmountOfTasks && (
          <AddButton as={Link} to="/addTask">+</AddButton>
        )}
        {this.props.tasks.length >= constants.maxAmountOfTasks && (
          <div>Możesz mieć maksymalnie {constants.maxAmountOfTasks} zadań jednocześnie!</div>
        )}
      </>
    );
  }
}

export default TodosPage;
