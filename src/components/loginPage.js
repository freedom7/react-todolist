import React from 'react';
import { Link } from 'react-router-dom'
import Input from './styled/input'
import Button from './styled/button'
import NavBar from './styled/navBar'

class LoginPage extends React.Component {
  render() {
    return (
      <>
        <NavBar>Logowanie</NavBar>
        <Input placeholder="login" onChange={this.props.handleLoginChange}></Input>
        <Button as={Link} to="/todos" onClick={this.props.handleLoginSubmit}>Zaloguj się</Button>
      </>
    );
  }
}

export default LoginPage;
