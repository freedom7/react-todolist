import React from 'react';
import { Link } from 'react-router-dom'
import Input from './styled/input'
import Button from './styled/button'
import NavBar from './styled/navBar'

class AddTaskPage extends React.Component {
  render() {
    return (
      <>
        <NavBar>Dodaj zadanie</NavBar>
        <Input placeholder="zadanie" onChange={this.props.handleTaskChange}></Input>
        <Button as={Link} to="/todos" onClick={this.props.submitTask}>Dodaj</Button>
      </>
    );
  }
}

export default AddTaskPage;
