import styled from 'styled-components'
const TaskContent = styled.div`
  display: inline-block;
  color: grey;
  font-size: 16px;
  text-align: justify;
`
export default TaskContent;