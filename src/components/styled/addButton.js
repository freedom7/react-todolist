import styled from 'styled-components'
const AddButton = styled.a`
  display: inline-block;
  margin: 1rem 1rem;
  border-radius: 50%;
  width: 40px;
  height: 40px;
  line-height: 32px;
  font-size: 40px;
  background: #998fef;
  color: white;
  border: 2px solid #4a3ad8;
  text-decoration: none;
`
export default AddButton;