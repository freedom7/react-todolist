import styled from 'styled-components'
const TaskNumber = styled.span`
  color: #4a3ad8;
  font-size: 30px;
  float: left;
  font-weight: bold;
`
export default TaskNumber;