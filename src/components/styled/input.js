import styled from 'styled-components'
const Input = styled.input`
  display: inline-block;
  border-radius: 5px;
  padding: 0.5rem 0.5rem;
  margin: 1rem 1rem;
  width: 20rem;
  font-size: 18.5px;
  background: white;
  color: black;
  border: 2px solid #4a3ad8;
`
export default Input;