import styled from 'styled-components'
const NavBar = styled.div`
  display: block;
  padding: 1rem 1rem;
  width: auto;
  background: #998fef;
  color: white;
  border-bottom: 2px solid #4a3ad8;
  font-size: 30px;
  margin-bottom: 2rem;
`
export default NavBar;