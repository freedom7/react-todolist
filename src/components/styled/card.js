import styled from 'styled-components'
const Card = styled.div`
  display: block;
  border-radius: 3px;
  padding: 0.5rem 0.5rem;
  margin: 0.5rem 1rem;
  width: auto;
  background: white;
  color: black;
  border: 2px solid #4a3ad8;
  `
export default Card;