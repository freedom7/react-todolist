import styled from 'styled-components'
const Button = styled.a`
  display: inline-block;
  border-radius: 30px;
  padding: 0.5rem 0;
  margin: 1rem 1rem;
  width: 11rem;
  background: #998fef;
  color: white;
  border: 2px solid #4a3ad8;
  text-decoration: none;
`
export default Button;