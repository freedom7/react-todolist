import styled from 'styled-components'
const LogoutButton = styled.a`
  display: inline-block;
  border-radius: 30px;
  padding: 0.5rem 0;
  width: 11rem;
  background: white;
  color: #4a3ad8;
  border: 2px solid #4a3ad8;
  text-decoration: none;
  font-size: 15px;
  float: right;
`
export default LogoutButton;