import styled from 'styled-components'
const LoadingDiv = styled.div`
  position:absolute;
  width: 100%;
  height: 100%;
  line-height: 222px;
  background: #bfb8ff;
  opacity: 0.5;
`
export default LoadingDiv;