import { put, takeEvery, all, select } from 'redux-saga/effects';

const getTaskText = (state) => state.taskText
const getTasks = (state) => state.tasks

function* submitTask() {
  yield put({type: 'setLoading', value: true});
  try {
    const resp = yield fetch('https://jsonplaceholder.typicode.com/todos', {
      method: 'POST',
      body: JSON.stringify({
        task: yield select(getTaskText),
      }),
      headers: {
        "Content-type": "application/json; charset=UTF-8"
      }
    })
    .then(resp => resp.json());
    yield put({ type: 'submitTaskCompleted', resp })
  }
  catch(error) {
    alert("Nie udało się dodać zadania!");
  }
  finally {
    yield put({type: 'setLoading', value: false});
  }
}

function* toggleTaskStatus(args) {
  args.event.preventDefault();
  yield put({type: 'setLoading', value: true});
  const tasks = yield select(getTasks);
  const newCompleted = !tasks.find((task)=> task.id === args.taskId).completed;
  try {
    const resp = yield fetch('https://jsonplaceholder.typicode.com/todos/'+args.taskId, {
      method: 'PUT',
      body: JSON.stringify({
        id: args.taskId,
        completed: newCompleted,
      }),
      headers: {
        "Content-type": "application/json; charset=UTF-8"
      }
    })
    .then(resp => resp.json())
    yield put({ type: 'toggleTaskStatusCompleted', resp })
  }
  catch(error) {
    alert("Nie udało się zmienić statusu zadania!");
  }
  finally {
    yield put({type: 'setLoading', value: false});
  }
}

function* removeTask(args) {
  const tasks = yield select(getTasks);
  const completed = tasks.find((task)=> task.id === args.taskId).completed
  if(completed){
    yield put({type: 'setLoading', value: true});
    try {
      const resp = yield fetch('https://jsonplaceholder.typicode.com/todos/'+args.taskId, {
        method: 'DELETE'
      })
      .then(resp => resp.json())
      yield put({ type: 'removeTaskCompleted', resp, taskId: args.taskId })
    }
    catch(error) {
      alert("Nie udało się usunąć zadania!");
    }
    finally {
      yield put({type: 'setLoading', value: false});
    }
  } else {
    alert("Możesz usunąć tylko zrobione zadania!");
  }
}

export function* rootSaga() {
  yield all([
    takeEvery('submitTask',submitTask),
    takeEvery('toggleTaskStatus',toggleTaskStatus),
    takeEvery('removeTask',removeTask)
  ]);
}